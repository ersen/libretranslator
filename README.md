# LibreTranslator

<img src="https://gitlab.com/BeowuIf/libretranslator/-/raw/master/app/src/main/ic_launcher-playstore.png" height=75px> **LibreTranslator a translator based on LibreTranslate**

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="60">](https://f-droid.org/packages/de.beowulf.libretranslater/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="60">](https://play.google.com/store/apps/details?id=de.beowulf.libretranslater)

## Features
- translate your text into up to 16 languages
- 5 different themes
- share translation easily from the app
- completely open source
- more coming

## Supported languages (UI)
- English
- German

## Supported languages (translation)
- Arabic
- Chinese
- English
- French
- German
- Hindi
- Indonesian
- Irish
- Italian
- Japanese
- Korean
- Polish
- Portuguese
- Russian
- Spanish
- Turkish
- Vietnamese

## Translation
The [LibreTranslate](https://libretranslate.de/) API is used for the translation. LibreTranslate is licensed under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)

## Contributing
See our [Contributing doc](CONTRIBUTING.md) for information on how to report issues or translate the app into your language.

## Wiki
In the [Wiki](https://gitlab.com/BeowuIf/libretranslator/-/wikis/home) you can find a short UI explanation to get started.

## Licensing
See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.
