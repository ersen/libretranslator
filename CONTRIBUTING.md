# Contributing

## Reporting issues

If you find an issue in the app, you can use our [Issue
Tracker](https://gitlab.com/BeowuIf/libretranslator/-/issues). Make sure that it
hasn't yet been reported by searching first.

Remember to include the following information:

* Android version
* Device model
* App version
* Steps to reproduce the issue

## Translating

You can translate the strings online at [POEditor](https://poeditor.com/join/project?hash=m73UT6rHqe).
 
