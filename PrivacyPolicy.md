# Privacy Policy
LibreTranslator was built as a free and open source application (app). This app is provided free of charge and is intended for use as is.  
This page is intended to inform you of the policies regarding the collection, use and disclosure of personal information should you choose to use this app.  
By using this app, you consent to the collection and use of information under this policy.

## Data we collect
We, the programmers of LibreTranslator, do not collect any type of data from users of this app.

## Data collected by others
Use of LibreTranslator may result in one or more of the following third party organisations collecting data from you. Please check their privacy policy for more information:
- [F-Droid](https://f-droid.org/en/about/), store where the app is published
- [Google PlayStore](https://policies.google.com/privacy), store where the app is published
- [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate), from whom we retrieve the translation data via their [API](https://libretranslate.de/docs/).  
  You can choose the LibreTranslate instance according to your preference.

In addition, in case you take part in the development of the app:
- [GitLab](https://about.gitlab.com/privacy/), whose platform we use to facilitate development of the app

## Policy Updates
This privacy policy may get modified from time to time.  
THis policy is effective as of 2021-11-22.

## Contact
Questions and suggestions about the LibreTranslator app can be sent to us via [email](mailto:contact-project+beowuif-libretranslator-28803428-issue-@incoming.gitlab.com) or directly through the [ticket system](https://gitlab.com/BeowuIf/libretranslator/-/issues).  
When you send an email, a confidential ticket is created in the GitLab repository and your email address is stored.
