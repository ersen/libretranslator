Features:
- translate your text into up to 16 languages
- 5 different themes
- share translation easily from the app
- completely open source
- more coming...

Supported languages (UI):
- English
- German

Supported languages (translation):
- Arabic
- Chinese
- English
- French
- German
- Hindi
- Indonesian
- Irish
- Italian
- Japanese
- Korean
- Polish
- Portuguese
- Russian
- Spanish
- Turkish
- Vietnamese

Translation:
The <a href="https://libretranslate.de/">LibreTranslate</a> API is used for the translation. LibreTranslate is licensed under <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">AGPLv3</a>.

Wiki:
In the <a href="https://gitlab.com/BeowuIf/libretranslator/-/wikis/home">Wiki</a> you can find a short UI explanation to get started.

Licensing:
Licensed under the <a href="https://gitlab.com/BeowuIf/libretranslator/-/blob/master/LICENSE">EUPL-1.2</a>.

OpenSource:
This app is <a href="https://gitlab.com/BeowuIf/libretranslator">open source</a>.
